﻿<?php

if (!isset($_REQUEST['action'])) {
    $_REQUEST['action'] = 'demandeConnexion';
}

$action = $_REQUEST['action'];
$idVisiteur = $_SESSION['idVisiteur'];
switch ($action) {
    case 'demandeConnexion': {
            include("views/v_connexion.php");
            break;
        }
    case 'valideConnexion': {
            $login = $_REQUEST['login'];
            $mdp = $_REQUEST['mdp'];
            $visiteur = $pdo->getInfosVisiteur($login, $mdp);
            if (!is_array($visiteur)) {
                ajouterErreur("Login ou mot de passe incorrect");
                include("views/v_erreurs.php");
                include("views/v_connexion.php");
            } else {
                $id = $visiteur['id'];
                $nom = $visiteur['nom'];
                $prenom = $visiteur['prenom'];
                connecter($id, $nom, $prenom);
                include("views/v_connexionReussi.php");
                include 'views/v_sommaire.php';
            }
            break;
        }

    case 'gestionFrais1': {
            include("views/v_formGestion1.php");
            break;
        }


    case 'gestionFrais2': {







            include("views/v_formGestion2.php");
            break;
        }
    case 'gestionFraisR': {
            include("views/v_formVerif.php");
            break;
        }

    case 'deconnexion': {
            include("views/v_deconnexion.php");
            break;
        }
}
